# Goodreads Bot

Hello 👋

## Demo

Here's a little demo screencast of it running

[Demo](demo/demo.mp4)

## The Stack

I chose PHP as the language, as I am most confident and comfortable in it. The Lumen framework was choosen for it is better fit for the task comparing to a full framework such as Laravel.

## System Requirements

The following are the System Requirements to run Lumen Framework
- PHP >= 7.2
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension

## Installation

Clone the application

```
git clone git@bitbucket.org:aharen/goodreads-bot.git
```

Install dependencies

```
cd goodreads-bot
composer install
```

Create the enviroment file from the provided example file

```
cp .env.example .env
```

The following keys in the environment file need to be updated

```
APP_KEY (application key)
FACEBOOK_TOKEN (facebook API token)
FACEBOOK_VERIFY_TOKEN (facebook webhook verification key)
GR_KEY (Goodreads API Key)
GR_SECRET (Goodreads API Secret)
IBM_API_KEY (Watson API Key)
```

Serve the application. You need to be inside the public folder to start the application.

```
cd public
php -S localhost:8080
```

The application can be accessed at http://localhost:8080

To setup and test the application, you will first need to create a new app at https://develpers.facebook.com and since you will need a public accessible domain you can use something similar to ngrok (https://ngrok.com/)

## Tests

From within the project folder run the following command to run the tests

```
./vendor/bin/phpunit
```

