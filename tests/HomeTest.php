<?php

class HomeTest extends TestCase
{
    public function test_visit_home()
    {
        $this->get('/');

        $this->assertEquals(
            'Hello from Goodreads API', $this->response->getContent()
        );
    }
}
