<?php

use App\Services\Goodreads;

class GoodreadsTest extends TestCase
{
    private $goodread;

    private $isbn = '0545265355';

    protected function setUp(): void
    {
        parent::setUp();

        $this->goodread = new Goodreads();
    }

    public function test_book_search()
    {
        $keyword = 'The Hunger Games (The Hunger Games, #1)';
        $resp = $this->goodread->search($keyword);
        $this->assertEquals(
            strtolower($keyword),
            $resp['search']['query']
        );
    }

    public function test_get_book()
    {
        $bookId = '7938275';
        $resp = $this->goodread->getBook($bookId);
        $this->assertEquals(
            $this->isbn,
            $resp['book']['isbn']
        );
    }

    public function test_get_book_reviews_no_reviews()
    {
        $resp = $this->goodread->getBookReviews($this->isbn);
        $this->assertNotEquals(
            0,
            count($resp)
        );
    }
}
