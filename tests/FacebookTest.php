<?php

use App\Services\Facebook;

class FacebookTest extends TestCase
{
    private $facebook;

    protected function setUp(): void
    {
        parent::setUp();

        $this->facebook = new Facebook();
    }

    public function test_get_user_first_name_invalid_type()
    {
        $this->expectException(TypeError::class);
        $this->facebook->getUserFirstName('abc');
    }

    public function test_get_user_first_name_invalid_id()
    {
        $this->assertStringContainsString(
            "Unsupported get request. Object with ID '123' does not exist",
            $this->facebook->getUserFirstName(123)
        );
    }

    public function test_get_user_first_name_valid_id()
    {
        $test_id = 10161090776065010;
        $getUserFirstName = $this->facebook->getUserFirstName($test_id);

        $this->assertEquals(
            'Ahmed',
            $getUserFirstName->first_name
        );

        $this->assertEquals(
            $test_id,
            $getUserFirstName->id
        );
    }
}
