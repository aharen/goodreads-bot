<?php

use App\Services\Goodreads;
use App\Services\Watson;
use App\Traits\ReadWriteTrait;

class WatsonTest extends TestCase
{
    use ReadWriteTrait;

    private $watson;

    private $goodread;

    private $isbn = '0545265355';

    private $key;

    protected function setUp(): void
    {
        parent::setUp();

        $this->watson = new Watson();
        $this->goodread = new Goodreads();
        $this->key = base64_encode($this->isbn);
        $bookReviews = $this->goodread->getBookReviews($this->isbn);
        $this->writeData($this->key, $bookReviews);
    }

    public function test_analyze()
    {
        $resp = $this->watson->semanticAnalyze($this->key);
        $this->assertEquals(
            'positive',
            $resp->sentiment->document->label
        );
    }
}
