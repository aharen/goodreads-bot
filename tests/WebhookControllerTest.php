<?php

class WebhookControllerTest extends TestCase
{
    public function test_setup_requires_resuest_parameters()
    {
        $resp = $this->json('GET', '/webhook');

        $resp->seeJson([
            "error" => [
                "message" => "Access Denied!",
            ],
        ]);

        $resp->assertResponseStatus(403);
    }

    public function test_setup_success()
    {
        $challenge = 1130158800;
        $query = http_build_query([
            'hub.mode' => 'subscribe',
            'hub.challenge' => $challenge,
            'hub.verify_token' => env('FACEBOOK_VERIFY_TOKEN'),
        ]);

        $resp = $this->get('/webhook?' . $query);
        $resp->assertResponseOk();

        $resp->seeJson([$challenge]);
    }

    public function test_empty_post()
    {
        $resp = $this->json('POST', '/webhook', []);

        $resp->seeJson([
            "error" => [
                "message" => "Not Found",
            ],
        ]);

        $resp->assertResponseStatus(404);
    }
}
