<?php

namespace App\Traits;

use App\Services\GoodReads;
use App\Services\Watson;

trait HandleActionsTrait
{
    protected function doGetStartedAction()
    {
        $user = $this->facebookService->getUserFirstName($this->userId);

        if ($user !== null) {
            $greet = $this->facebookService->greetTheUser($this->userId, $user->first_name);
            $button_template = $this->facebookService->sendButtonTemplate($this->userId);
        }

        return response('OK');
    }

    protected function doSearchByBookName()
    {
        $this->facebookService->sendMessageToUser($this->userId, 'Whats the books name?');
        $this->writeData('user-' . $this->userId, ['search' => 'name']);
        return response('OK');
    }

    protected function doSearchByBookId()
    {
        $this->facebookService->sendMessageToUser($this->userId, 'Whats the books id?');
        $this->writeData('user-' . $this->userId, ['search' => 'id']);
        return response('OK');
    }

    protected function doEvaluation()
    {
        $payload = $this->event['postback']['payload'];

        // see if evaluation on going
        if (isset($this->userData['evaluating']) && $this->userData['evaluating'] === $payload) {
            return response('OK');
        }

        // start evaluation
        $this->userData['evaluating'] = $payload;
        $this->writeData('user-' . $this->userId, $this->userData);

        $this->facebookService->sendMessageToUser($this->userId, 'Evaluating your choice, this may take sometime..');

        $book_id = str_replace('getBookReview-', '', $payload);
        $book = $this->getBook($book_id);

        if ($book !== null) {
            $this->facebookService->sendMessageToUser($this->userId, 'Interesting choice ' . $book['book']['title']);
            $reviews = $this->getBookReviewsAndWrite($book['book']['isbn']);

            if (count($reviews) === 0) {
                $this->facebookService->sendMessageToUser($this->userId, 'Please hold on a bit longer as I analyze the data');
                $watson = new Watson();
                $isbn = base64_encode($book['book']['isbn']);
                $analyze = $watson->semanticAnalyze($isbn);

                if ($analyze) {
                    return $this->analyzedVerdict($analyze->sentiment->document->label);

                } else {
                    $this->facebookService->sendMessageToUser($this->userId, 'I\'m sorry, but I am unable to analyze that data');
                }

            } else {
                $this->facebookService->sendMessageToUser($this->userId, 'Hmm.. no one has reviewed this book. Would you read it and let us know how it is?');
            }

        } else {
            $this->facebookService->sendMessageToUser($this->userId, 'Oh No! I cant find that book!');
        }
        return response('OK');
    }

    private function analyzedVerdict($label)
    {
        if ($label === 'positive') {
            $this->facebookService->sendMessageToUser($this->userId, 'I would recommend you buy this book, it seems an epic read. Enjoy!');

        } else if ($label === 'neutral') {
            $this->facebookService->sendMessageToUser($this->userId, 'Hmm.. I cant seem to be able to make up my mind.');

        } else {
            $this->facebookService->sendMessageToUser($this->userId, 'Seems like this book aint that great, wanna try another book?');
        }

        return response('OK');
    }

    public function getBook($book_id)
    {
        $gr = new GoodReads();
        $book = $gr->getBook($book_id);

        return $book;
    }

    public function getBookReviewsAndWrite($isbn)
    {
        $gr = new GoodReads();
        $reviews = $gr->getBookReviews($isbn);

        $this->writeData(base64_encode($isbn), $reviews);

        return $reviews;
    }
}
