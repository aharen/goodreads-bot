<?php

namespace App\Traits;

use Cache;

trait ReadWriteTrait
{
    protected function readData($filename, $sort = true)
    {
        $data = Cache::get($filename);
        if (!is_array($data)) {
            $data = unserialize($data);

            if ($data === false) {
                return null;
            }
        }

        if ($sort) {
            $data = array_slice($data, 0, 50);
            return $this->multiDimentionalArraySortByValue($data, 'date');
        }

        return $data;
    }

    protected function writeData($key, $data)
    {
        Cache::forever($key, serialize($data));
    }

    protected function multiDimentionalArraySortByValue($data, $key)
    {
        $dates = array_column($data, $key);
        array_multisort($dates, SORT_DESC, $data);
        return $data;
    }
}
