<?php

namespace App\Http\Controllers;

use App\Traits\ReadWriteTrait;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use ReadWriteTrait;

    protected function respondError($message, $code = 403)
    {
        return response()
            ->json([
                'error' => [
                    'message' => $message,
                ],
            ], $code);
    }
}
