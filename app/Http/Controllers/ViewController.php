<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ViewController extends Controller
{
    public function __invoke(Request $request)
    {
        $id = $request->get('id');

        if ($id !== null) {
            $data = $this->readData($id);
            $items = collect($data);

            return response()->json(
                $items->sortByDesc('date')->pluck('text')->take(20)->toArray()
            );
        }

        return $this->respondError('Not Found', 404);
    }
}
