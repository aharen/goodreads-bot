<?php

namespace App\Http\Controllers;

use App\Services\Facebook;
use App\Services\GoodReads;
use App\Traits\HandleActionsTrait;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
    use HandleActionsTrait;

    private $event;

    private $facebookService;

    private $userId;

    private $userData;

    public function get(Request $request)
    {
        if ($request->has('hub_verify_token') && $request->has('hub_mode')) {
            if ($request->get('hub_mode') === 'subscribe' && $request->get('hub_verify_token') === env('FACEBOOK_VERIFY_TOKEN')) {
                return response()
                    ->json((int) $request->get('hub_challenge'));
            }
        }
        return $this->respondError('Access Denied!');
    }

    public function post(Request $request)
    {
        if ($request->has('object') && $request->object === 'page') {
            $this->facebookService = new Facebook();

            return collect($request->entry)->each(function ($entry) {
                $this->event = $this->getEvent($entry);
                $this->userId = $this->event['sender']['id'];
                $this->userData = $this->readUserInfo($this->userId);

                if (isset($this->event['message'])) {
                    return $this->handleSearch();

                } else {
                    return $this->handleActions();
                }
            });

            return response('OK');
        }
        return $this->respondError('Not Found', 404);
    }

    private function handleSearch()
    {
        $keyword = $this->event['message']['text'];

        $gr = new GoodReads();
        $search = $gr->search($keyword);

        if ((int) $search['search']['total-results'] !== 0) {
            $resultsAll = $search['search']['results']['work'];
            $results = array_slice($resultsAll, 0, 9);

            $elements = [];
            foreach ($results as $result) {
                if (isset($result['best_book'])) {
                    $elements[] = $this->facebookService->addListTemplateElement($result['best_book']);
                }
            }

            if (count($elements) > 0) {
                $this->facebookService->sendMessageToUser($this->userId, 'Here are top results for  ' . $keyword . ' from Goodreads');
                $this->facebookService->sendListTemplate($this->userId, $elements);
                return response('OK');
            }
        }

        $this->facebookService->sendMessageToUser($this->userId, 'Sorry, I couldn\'t find any thing for  ' . $keyword);
        return response('OK');

    }

    private function handleActions()
    {
        switch ($this->event['postback']['payload']) {
            case 'getStartedAction':
                return $this->doGetStartedAction();
                break;

            case 'searchByBookName':
                return $this->doSearchByBookName();
                break;

            case 'searchByBookId':
                return $this->doSearchByBookId();
                break;

            default:
                if (strpos($this->event['postback']['payload'], 'getBookReview-') === 0) {
                    return $this->doEvaluation();
                }
                break;
        }

        $this->facebookService->sendMessageToUser($this->userId, 'Hmmm.. Sorry but I couldn\'t understand you, try again please :)');
        return response('OK');
    }

    private function readUserInfo($uid)
    {
        return $this->readData('user-' . $uid, false);
    }

    private function getEvent($entry)
    {
        return $entry['messaging'][0];
    }
}
