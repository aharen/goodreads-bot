<?php

namespace App\Services;

use App\Traits\ReadWriteTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class Watson
{
    use ReadWriteTrait;

    protected $host = 'https://gateway.watsonplatform.net/natural-language-understanding/api/v1/analyze';

    protected $client;

    protected $api_key;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => $this->host,
            'headers' => [
                'Content-Type' => 'application/json',
                'Content-Type' => 'charset=utf-8',
                'Accept' => 'application/json',
                'Cache-Control' => 'no-cache',
                'Authorization' => 'Basic ' . base64_encode('apikey:' . env('IBM_API_KEY')),
            ],
        ]);
    }

    public function semanticAnalyze($id)
    {
        $text = $this->getText($id);
        return $this->get([
            'version' => '2019-07-12',
            'text' => urlencode($text),
            'features' => 'sentiment,semantic_roles',
            'return_analyzed_text' => true,
        ]);
    }

    private function getText($id)
    {
        $data = $this->readData($id);
        $collection = collect($data);
        $items = $collection->sortByDesc('date')->pluck('text')->take(5)->toArray();
        return implode(' ', $items);
    }

    protected function get($data)
    {
        try {
            $response = $this->client->get('analyze', ['query' => $data]);
            $body = $response->getBody();
            $data = json_decode($body->getContents());
        } catch (RequestException $e) {
            $data = $e->getMessage();
        } catch (ClientException $e) {
            $data = $e->getMessage();
        }
        return $data;
    }
}
