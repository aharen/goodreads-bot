<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class Facebook
{
    private $host = 'https://graph.facebook.com/';

    private $client;

    private $data;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => $this->host,
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . env('FACEBOOK_TOKEN'),
            ],
        ]);
        return $this;
    }

    public function getUserFirstName(int $user_id)
    {
        $this->get(
            'v7.0/' . $user_id . '?fields=first_name'
        );

        return $this->data;
    }

    public function toArray(): array
    {
        return (array) $this->data;
    }

    public function greetTheUser($user_id, $first_name)
    {
        return $this->sendMessageToUser(
            $user_id,
            "Hello $first_name! \n\nI am Goodreads Bot! I can help you evaluate awesome books!"
        );
    }

    public function sendMessageToUser($user_id, $message)
    {
        $uri = 'v7.0/me/messages';
        $data = [
            'form_params' => [
                'messaging_type' => 'RESPONSE',
                'recipient' => [
                    'id' => $user_id,
                ],
                'message' => [
                    'text' => $message,
                ],
                "postback",
            ],
        ];
        return $this->post($uri, $data);
    }

    public function sendListTemplate($user_id, $elements)
    {
        $payload = [
            'template_type' => 'generic',
            'elements' => $elements,
        ];

        return $this->sendTemplate($user_id, $payload);
    }

    public function addListTemplateElement($element)
    {
        return [
            "title" => $element['title'],
            "subtitle" => 'by ' . $element['author']['name'],
            "image_url" => $element['image_url'],
            "buttons" => [
                [
                    'type' => 'postback',
                    'title' => 'Evaluate',
                    'payload' => 'getBookReview-' . $element['id'],
                ],
            ],
        ];
    }

    public function sendButtonTemplate($user_id)
    {
        $payload = [
            'template_type' => 'button',
            'text' => 'How would you like to search for a book?',
            'buttons' => [
                [
                    'type' => 'postback',
                    'title' => 'Book Name',
                    'payload' => 'searchByBookName',
                ], [
                    'type' => 'postback',
                    'title' => 'GoodReads Id',
                    'payload' => 'searchByBookId',
                ],
            ],
        ];

        return $this->sendTemplate($user_id, $payload);
    }

    public function sendTemplate($user_id, $payload)
    {
        $uri = 'v7.0/me/messages';
        $data = [
            'form_params' => [
                'recipient' => [
                    'id' => $user_id,
                ],
                'message' => [
                    'attachment' => [
                        'type' => 'template',
                        'payload' => $payload,
                    ],
                ],
            ],
        ];
        return $this->post($uri, $data);
    }

    protected function post($method, $data)
    {
        try {
            $response = $this->client->post($method, $data);
            $body = $response->getBody();
            $this->data = json_decode($body->getContents());

        } catch (RequestException $e) {
            $this->data = $e->getMessage();

        } catch (ClientException $e) {
            $this->data = $e->getMessage();

        }
        return $this->data;
    }

    protected function get($method)
    {
        try {
            $response = $this->client->get($method);
            $body = $response->getBody();
            $this->data = json_decode($body->getContents());

        } catch (RequestException $e) {
            $this->data = $e->getMessage();

        } catch (ClientException $e) {
            $this->data = $e->getMessage();

        }
        return $this->data;
    }

}
