<?php

$router->get('/', function () use ($router) {
    return 'Hello from Goodreads API';
});

$router->get('/webhook', 'WebhookController@get');
$router->post('/webhook', 'WebhookController@post');

$router->get('/view', 'ViewController');
